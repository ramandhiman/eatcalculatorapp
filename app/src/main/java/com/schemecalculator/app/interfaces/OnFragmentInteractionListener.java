package com.schemecalculator.app.interfaces;

public interface OnFragmentInteractionListener {
    public void changeFragment(int id);
}