package com.schemecalculator.app.interfaces;

public interface OnBackFragmentListener {
    public void backFragment(int back_id);
}
