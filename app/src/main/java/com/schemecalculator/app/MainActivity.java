package com.schemecalculator.app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = MainActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = MainActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtEatOutTV)
    TextView txtEatOutTV;
    @BindView(R.id.editHeadsET)
    TextView editHeadsET;
    @BindView(R.id.editTotalCostET)
    TextView editTotalCostET;
    @BindView(R.id.editExemptItemsET)
    TextView editExemptItemsET;
    @BindView(R.id.txtCalculateTV)
    TextView txtCalculateTV;
    @BindView(R.id.txtDiscountableTV)
    TextView txtDiscountableTV;
    @BindView(R.id.txtDiscountTV)
    TextView txtDiscountTV;
    @BindView(R.id.txtFinalCostTV)
    TextView txtFinalCostTV;
    @BindView(R.id.privacyRL)
    RelativeLayout privacyRL;
    @BindView(R.id.termsRL)
    RelativeLayout termsRL;
    @BindView(R.id.dayDateTV)
    TextView dayDateTV;
    float discount;
    float discountable;
    float final_cost;
    int no_of_heads;
    float total_cost;
    float exempt;
    SimpleDateFormat form;
//    String temp = "£";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//      editTotalCostET.setText(temp);
//      editExemptItemsET.setText(temp);


        Date today = Calendar.getInstance().getTime();//getting date
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM,EEEE");//formating according to my need
        String date = formatter.format(today);
        dayDateTV.setText(date);

        Date day = Calendar.getInstance().getTime();//getting date
        form = new SimpleDateFormat("EEEE");//formating according to my need
        String day_of_month = form.format(day);
    }


    @OnClick({R.id.txtCalculateTV, R.id.privacyRL, R.id.termsRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCalculateTV:
                hideKeybaord(view);
                performCalculateClick();
                break;
            case R.id.privacyRL:
                performPrivacyClick();
                break;
            case R.id.termsRL:
                performTermsClick();
                break;
        }
    }

    private void performPrivacyClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        startActivity(browserIntent);
    }

    private void performTermsClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        startActivity(browserIntent);
    }

    private void performCalculateClick() {
        if (isValidate()) {
            no_of_heads = Integer.parseInt(editHeadsET.getText().toString().trim());

            total_cost = Float.parseFloat(editTotalCostET.getText().toString().replace("£","").trim());
            exempt = Float.parseFloat(editExemptItemsET.getText().toString().replace("£","").trim());

            discountable = total_cost - exempt;
           // if (form.equals("Monday") || form.equals("Tuesday") || form.equals("Wednesday")) {
                if ((discountable / no_of_heads) >= 20) {
                    discount = no_of_heads * 10;
                } else {
                    discount = discountable / 2;
                }
//            } else {
//                discount = discountable / 2;
//            }
            final_cost = total_cost - discount;


            double d_discountable = 0.00;
            double d_discount = 0.00;
            double d_final_cost = 0.00;

            d_discountable = Double.parseDouble(String.valueOf(discountable));
            d_discount = Double.parseDouble(String.valueOf(discount));
            d_final_cost = Double.parseDouble(String.valueOf(final_cost));
            String strDiscountable = String.format("%.2f", d_discountable);
            String strDiscount = String.format("%.2f", d_discount);
            String strFinalCost = String.format("%.2f", d_final_cost);


            txtDiscountableTV.setText("£".concat(strDiscountable));
            txtDiscountTV.setText("£".concat(strDiscount));
            txtFinalCostTV.setText("£".concat(strFinalCost));
        }
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editHeadsET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string._enter_heads));
            flag = false;
        } else if (editTotalCostET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string._enter_total_cost));
            flag = false;
        } else if (editExemptItemsET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string._enter_exempt));
            flag = false;
        }
        return flag;
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void hideKeybaord(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(),0);
    }
}
